(await (async () => {
    const composeOPML = data => 
        `<?xml version="1.0"?>
<opml version="1.0">
    <head>
        <title>Google Podcasts Subscriptions</title>
    </head>
    <body>
    ${data.map(outline => `<outline type="rss" text="${outline.text}" title="${outline.title}" xmlUrl="${outline.feed}" htmlUrl="${outline.home}" />`).join('\n')}
    </body>
</opml>`

    const downloadOPMLFile = text => {
        const blob = new Blob([text], {ytpe:"terxt/plain"})
        const link = document.createElement('a')
        const url = URL.createObjectURL(blob)
        link.href = url
        link.download = 'GooglePodcastSubscriptions.opml'
        link.click()
        URL.revokeObjectURL(url)
    }

    const getSuscriptionData = async elem => {
        const doc = await (new DOMParser()).parseFromString(await (await fetch(elem.href)).text(), 'text/html')
        const container = Array.from(doc.body.children).filter(
            e => e?.tagName === 'C-WIZ'
        )?.[0]?.children?.[0]?.children?.[1]?.children?.[0]?.children?.[0]?.children?.[0]
        return {
            title: container?.children?.[0]?.innerText ?? '',
            text: container?.children?.[4]?.innerText ?? '',
            feed: container?.children?.[2]?.children?.[1].getAttribute('jsdata')?.split(';')?.[3] ?? '',
            home: container?.children?.[3]?.children?.[0].href,
        }
    }

    const getSuscriptionItems = () => Array.from(
        Array.from(document.body.children).filter(
            e => e?.tagName === 'C-WIZ'
        )?.[0]?.children?.[0]?.children?.[1]?.children?.[1]?.children?.[1]?.children
    )

    downloadOPMLFile(composeOPML(await Promise.all(getSuscriptionItems()?.map(getSuscriptionData))))
}))()