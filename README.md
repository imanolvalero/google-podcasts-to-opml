# Google Podcast OPML extractor

Google Podcasts has not any way to export suscriptions to OPML.

This script exports your suscriptions to OPML.

## Steps for extract your suscriptions
1. Open [Google Podcasts](https://podcasts.google.com) in your favorite browser, and login in the service.
2. Select **Suscriptions** in the menu on the left part of the screen.
3. Open **DevTools** and select **Console** tab.
4. Reload the page.
5. Copy the content of the `main.js` file to **clipboard**
6. Paste the content of the **cipboard** in **Console** of **DevTools**, and pres `Enter` to execute it.

If everything goes right, the browser will download an OPML file with your suscriptions, ready to be imported by other podcast clients.


## Notes
- Please, keep ion mind that this scrap function, and it relies on current (**2023-01-11**) layout of Goolge Podcasts, I cannot ensure it will work if Google changes its application layout.

- This is a quick and dirty solution for helping a colleage. I know I'm a bit of a witch, but please, don't burn me for publish this code.

- Keep in mind also, this code has been released under **GPL3** license. **You undestand an accept the conditions of this license if you clone or modify this code**.
